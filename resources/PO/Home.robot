*** Settings ***
Library    SeleniumLibrary
Library    String

*** Variables ***
${HOME_URL}                 https://www.amazon.com.br/
${HOME_TITLE}               Amazon.com.br | Tudo pra você, de A a Z.
${HOME_FIELD_PESQUISAR}     name=search_query
${HOME_BTN_PESQUISAR}       name=submit_search
${HOME_TOPMENU}             xpath=//div[@class='nav-sprite']
${HOME_PRODUCT}             xpath=//*[@id="center_column"]//img[@alt="Faded Short Sleeve T-shirts"]
${HOME_BTN_ADDCART}         xpath=//*[@id="add_to_cart"]/button
${HOME_BTN_CHECKOUT}        xpath=//*[@id="layer_cart"]//a[@title="Proceed to checkout"]

*** Keywords ***
#### Ações
Acessar a página home do site
    Go To               ${HOME_URL}
    Wait Until Element Is Visible    ${HOME_TOPMENU}
    Title Should Be     ${HOME_TITLE}

Adicionar o produto "${PRODUTO}" no carrinho
    Input Text            id:twotabsearchtextbox    text=${PRODUTO}
    Click Element         id:nav-search-submit-button
    Wait Until Element Is Visible    locator=//span[@class='a-size-base-plus a-color-base a-text-normal'][contains(.,'T-SHIRT - QUEIRA O BEM, PLANTE O BEM, O RESTO VEM')]
    Click Element                    locator=//span[@class='a-size-base-plus a-color-base a-text-normal'][contains(.,'T-SHIRT - QUEIRA O BEM, PLANTE O BEM, O RESTO VEM')]
    Click Button                     locator=add-to-cart-button

# Digitar o nome do produto "${PRODUTO}" no campo de pesquisa
#     Input Text          ${HOME_FIELD_PESQUISAR}    ${PRODUTO}

# Clicar no botão pesquisar
#     Click Element       ${HOME_BTN_PESQUISAR}

# Clicar no botão "Add to Cart" do produto
#     Wait Until Element Is Visible   ${HOME_PRODUCT}
#     Click Element                   ${HOME_PRODUCT}
#     Wait Until Element Is Visible   ${HOME_BTN_ADDCART}
#     Click Button                    ${HOME_BTN_ADDCART}

# Clicar no botão "Proceed to checkout"
#     Wait Until Element Is Visible   ${HOME_BTN_CHECKOUT}
#     Click Element                   ${HOME_BTN_CHECKOUT}
